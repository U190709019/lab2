public class FindGrade {
	
	public static void main(String[] args) {
		int Score =  Integer.parseInt(args[0]);
		
		if (90 <= Score && Score <= 100) {
			System.out.println("Your grade is A");
		} else if (80 <= Score && Score < 90) {
			System.out.println("Your grade is B");
		} else if (70 <= Score && Score < 80) {
			System.out.println("Your grade is C");
		} else if (60 <= Score && Score < 70) {
			System.out.println("Your grade is D");
		} else if (Score < 60 && Score >= 0) {
			System.out.println("Your grade is F");
		} else {
			System.out.println("It is not a valid score!");
		}
	}
}
